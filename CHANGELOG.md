#### 0.4.1 (2020-09-02)

##### Build System / Dependencies

*  Публичный билд v0.4.1 (8c0cba45)

##### Documentation Changes

*  add CHANGELOG (85493db9)
*  add CHANGELOG (1c15fd44)

#### 0.4.2 (2020-09-02)

#### 0.5.2 (2021-09-08)

#### 0.6 (2021-11-23)
* Переименование в create-pe-react-app. 
* Можно точно так же, как в create-react-app, генерировать новые проекты.
