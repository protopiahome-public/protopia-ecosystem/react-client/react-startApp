#!/usr/bin/env node

const fs = require('fs');
const fse = require('fs-extra');

const time = new Date();

const walk = function (dir) {
  let results = [];
  let list = fs.readdirSync(dir);
  list.forEach((file) => {
    file = `${dir}/${file}`;
    let stat = fs.statSync(file);
    if (stat && stat.isDirectory()) {
      /* Recurse into a subdirectory */
      results = results.concat(walk(file));
    } else {
      /* Is a file */
      results.push(file);
    }
  });
  return results;
};

console.log('PE installing');

fs.mkdirSync('pe-app');
process.chdir(process.cwd() + '/' + 'pe-app');

fse.copySync(__dirname + '/../template', process.cwd());
const files = walk(process.cwd());
for (const i in files) {
  const file = files[i];
  if (file.match(/node_modules/)) {
    continue;
  }
  fs.utimesSync(file, time, time);
  if (file.match(/\.npmignore$/)) {
    fs.renameSync(file, file.replace(/\.npmignore$/, '.gitignore'));
  }
}

require('child_process').execSync(
    'npm i',
    {stdio: 'inherit'}
);

console.log('PE created');
